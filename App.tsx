import React from 'react';

// firebase
import './src/config/firebase'
import RootNavigation from './src/navigate/RootNavigation';

export default function App() {
  return (  
    <RootNavigation />
  );
};
