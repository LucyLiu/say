import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { getProfile } from '../../module';
import { UserInfo } from '../auth/LoginScreen';
const SettingScreen = () => {
  const [currentUser, setCurrentUser] = useState<UserInfo>({});
  useEffect(() => {
    getProfile((result: any) => {
      setCurrentUser(result);
    });
  }, [])
  useEffect(() => {
    console.log('currentUser', currentUser);
  }, [currentUser])
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{marginTop: 10, justifyContent: 'center', alignItems: 'center'}}>
        <Image style={{width: 100, height: 100, borderRadius: 50}} source={{uri: `${currentUser.photoURL}`}}/>
        <Text style={{marginTop: 10}}>{currentUser.displayName}</Text>
      </View>
    </SafeAreaView>
  )
}

export default SettingScreen

const styles = StyleSheet.create({})