import React, { useCallback, useEffect, useState } from 'react';
import { Alert, StyleSheet, Text, TouchableOpacity, View, Button } from 'react-native';
import * as WebBrowser from 'expo-web-browser';
import * as Google from 'expo-auth-session/providers/google';
import { getAuth, GoogleAuthProvider, onAuthStateChanged, signInWithCredential, signOut } from 'firebase/auth';
import { createProfile } from '../../module';
import AsyncStorage from '@react-native-async-storage/async-storage';

WebBrowser.maybeCompleteAuthSession();

export interface UserInfo {
  displayName?: string | null,
  email?: string | null,
  phoneNumber?: string | null,
  photoURL?: string | null,
  providerId?: string | null,
  uid?: string | null,
  id?: string | null
}

const LoginScreen = ({navigation}: any) => {
  const [userInfo, setUserInfo] = useState<UserInfo | null>(null)  

  const [request, response, promptAsync] = Google.useIdTokenAuthRequest(
    {
      clientId: '959629486417-2vd0i2dba3b2diblig4si84lqqtb8qih.apps.googleusercontent.com',
    },
  );

  const signInWithGoogle = useCallback((credential: any) => {
    const auth = getAuth();
    signInWithCredential(auth, credential);
    onAuthStateChanged(auth, async(user: any) => {
      if (user != null) {
        setUserInfo({...user.providerData[0], id: user.uid})
        createProfile({...user.providerData[0], id: user.uid});
        await AsyncStorage.setItem('Auth', JSON.stringify({
          currentUserId: user.uid
        }))
        navigation.navigate("RootStack")
      }    
    });
  }, [])

  useEffect(() => {
    if (response?.type === 'success') {
      const { id_token } = response.params;      
      const auth = getAuth();
      const provider = new GoogleAuthProvider();
      const credential = GoogleAuthProvider.credential(id_token);
      signInWithGoogle(credential);
    }
  }, [response]);
  
  return (
    <View style={styles.container}>
      <Button
        disabled={!request}
        title="Google Login"
        onPress={() => {
          promptAsync();
        }}
      />
    </View>
  )
}

export default LoginScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})