import AsyncStorage from "@react-native-async-storage/async-storage";
import { getAuth } from "firebase/auth";

let sharedCurrentUserId: string | null;

export const getCurrentUserId = async(): Promise<string | null> => {
  const { currentUser } = getAuth();
  if (!currentUser) return null;

  // try read sharedCurrentUserId from storage
  if (sharedCurrentUserId === undefined) {
    const persistedAuthData = await AsyncStorage.getItem('Auth');
    if (persistedAuthData) {
      const { currentUserId } = JSON.parse(persistedAuthData) || {};
      sharedCurrentUserId = currentUserId || null;
    } else {
      sharedCurrentUserId = null;
    }
  }

  return sharedCurrentUserId || currentUser.uid;
}