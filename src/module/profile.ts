import { collection, doc, getDoc, setDoc } from "firebase/firestore";
import { db } from "../config/firebase";
import { UserInfo } from "../screen/auth/LoginScreen";
import { getCurrentUserId } from "./auth";

const profileRef = collection(db, 'profile');

export const profileDoc = (id: any) => {
  return doc(profileRef, id);
}

export const createProfile = async(userInfo: UserInfo) => {
  if(userInfo.id === null) return;
  try {
    await setDoc(profileDoc(userInfo.id), userInfo);
  } catch {}
}

export const getProfile = async(callback: (result: Object) => void)=> {
  const myId = await getCurrentUserId();
  try {
    const snap = await getDoc(profileDoc(myId));
    if(snap.exists()) {
      callback({
        ...snap.data(),
        id: snap.id
      })
    } else {
      console.log('沒有這位使用者')
    }
  } catch {}
}