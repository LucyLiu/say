import { initializeApp } from 'firebase/app';
import { getAuth } from '@firebase/auth';
import { getDatabase } from '@firebase/database';
import { getFirestore } from "firebase/firestore";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  initializeAuth,
  getReactNativePersistence
} from 'firebase/auth/react-native';
const firebaseConfig = {
  apiKey: "AIzaSyCalxgh9yzCTol8n6Qsz3bc_w__ftL67mI",
  authDomain: "saying-3283a.firebaseapp.com",
  projectId: "saying-3283a",
  storageBucket: "saying-3283a.appspot.com",
  messagingSenderId: "959629486417",
  appId: "1:959629486417:web:b65c9fbd6ab95e9010cb70"
};

const app = initializeApp(firebaseConfig);
const auth = initializeAuth(app, {
  persistence: getReactNativePersistence(AsyncStorage)
});
const db = getFirestore(app);

export { auth, db };
