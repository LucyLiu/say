export const GOOGLE_IOS_CLIENT_ID = (() => {
  return '959629486417-0rcc0ukct28nhm4gqndtlo12kbslo7pf.apps.googleusercontent.com'
})()

export const GOOGLE_ANDROID_CLIENT_ID = (() => {
  return '959629486417-crtcv2k5inujkep75cg8345ldav8ufrf.apps.googleusercontent.com'
})()

export const GOOGLE_WEB_CLIENT_ID = (() => {
  return '959629486417-2vd0i2dba3b2diblig4si84lqqtb8qih.apps.googleusercontent.com'
})()

export const GOOGLE_CLIENT_ID = (() => {
  return '959629486417-f5ka5700qgcii5g93n97rh1i65h2bp6n.apps.googleusercontent.com'
})()