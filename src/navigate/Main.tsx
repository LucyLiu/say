import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen, SettingScreen } from '../screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MyArticle from '../screen/article/MyArticle';
import CreateArticle from '../screen/article/CreateArticle';

const BottomTab = createBottomTabNavigator();
const MainStack = createStackNavigator();

const BottomTabNavigation = () => {
  return (
    <BottomTab.Navigator screenOptions={{
      tabBarIconStyle: {backgroundColor: 'red'}      
    }}>
      <BottomTab.Screen 
        name="Home" 
        component={HomeScreen}
        options={{
          tabBarIcon: ({color}) => <Icon color={color} name="home" size={18}/>,
          title: "首頁"        
        }}
      />
      <BottomTab.Screen 
        name="MyArticle"
        component={MyArticle}
        options={{
          tabBarIcon: ({color}) => <Icon color={color} name="sticky-note" size={18}/>,
          title: "我的文章"
        }}
      />
      <BottomTab.Screen 
        name="CreateArticle"
        component={CreateArticle}
        options={{
          tabBarIcon: ({color}) => <Icon color={color} name="plus" size={18} />,
          title: "新增文章"
        }}
      />
      <BottomTab.Screen 
        name="Setting" 
        component={SettingScreen} 
        options={{
          tabBarIcon: ({color}) =>  <Icon color={color} name="cog" size={18}/>,
          title: "設定"
        }}
      />
    </BottomTab.Navigator>
  )
}

const Main = () => {
  return (
    <MainStack.Navigator screenOptions={{
      headerShown: false
    }}>
      <MainStack.Screen name="BottomTab" component={BottomTabNavigation} />
    </MainStack.Navigator>
  )
}

export default Main

const styles = StyleSheet.create({})